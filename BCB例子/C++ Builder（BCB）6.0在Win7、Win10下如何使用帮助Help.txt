C++ Builder（BCB）6.0在Win7、Win10下如何使用帮助Help

（win10系统的可以搜索下载：win10的winhlp32解決方案，也可以从winxp中windows目录下复制一个winhlp32.exe到win10的对应windows目录下）

因为Win7、Win8、Win8.1不再支持.hlp帮助类型，在BCB中无法打开帮助Help，打开时出错："不包括功能"或"不受支持的帮助"，提示如下：
为何无法获取关于此程序的帮助信息？
此程序的帮助信息采用 Windows 帮助格式创建，该格式依赖于未包含在此版本的 Windows 中的一项功能。不过，您可以下载一个将允许您查看采用 Windows 帮助格式创建的帮助信息的程序。
有关详细信息，请转至 Microsoft 帮助和支持网站。

M$提供了一个单独的Windows帮助程序（WinHlp32.exe）下载安装，https://support.microsoft.com/zh-cn/kb/917607 ，分为x86和x64两个版本，分别对应32位和64位Windows系统。

在C++ Builder中使用帮助很简单，在代码编辑窗口中，把光标停留在变量、类型、函数或者关键字上面，按F1键，就会打开帮助窗口，显示找到的主题。需要注意的是，BCB 6.0提供了两种类库，一种就是著名的我们常用的VCL（Visual Component Library），一种是用于跨平台的CLX（Component Library for Cross-Platform），所以帮助主题里面一般也会显示对应于VCL和CLX的并列的两条，选择注有Visual Component Library Reference或非CLX的那条就可以了。

如果我们想要查看某个控件的属性和方法，也可以打开【C++Builder 帮助】菜单，选择【索引】页面，比如Memo、ListBox控件，它们的类类型为TMemo、TListBox，所以要输入TMemo或者TListBox，打开帮助主题，就有Properties（属性）、Methods（方法）、Events（事件）等链接以及Description描述，点击Properties，就打开了属性列表的帮助窗口，可以按照字母顺序排列（点击Alphabetically），也可以按照继承树的对象排列（点击By Object）。方法和事件列表窗口也是类似的。

BCB 6.0的帮助里面还有一个【Windows SDK】菜单，可以查看Windows API函数、Windows系统定义的一些常量以及一些特定的主题等。

学习C++ Builder入门以后，最好和最完整的文档资料就是帮助Help了，而且控件的有些属性和方法是有Example（例子）的，是很简短的代码，看一看就知道该属性或方法具体怎么用了。因此，要学会使用帮助，善用帮助。不过，帮助都是英文的，一般大学英语四级是没有问题的，可以安装一个有道词典来查生词，而且经常查阅英文帮助也有利于锻炼英语水平。毕竟计算机技术大多数一手的资料都是英文的。

PS：因为现在的屏幕分辨率都比较高，帮助窗口里默认显示的文字字体就偏小了，可以在选项菜单里把字体设置为大，才是正常的大小。
