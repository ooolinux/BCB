关于C++ Builder（RAD Studio）的代码提示问题

C++ Builder（RAD Studio） 10.3.x和10.4代码提示用了临时新技术（64位的cquery.exe独立进程）有问题又很慢，几乎不可用，很多人都是用10.2.x，期待10.5会集成Visual Assist，不知道那时候代码提示能不能像Visual Studio一样好用。

用10.3.x或10.4的可以每次新建项目设置为经典编译器，代码提示（使用传统技术）会好很多。空项目设置以后可以作为项目模板使用。设置方法：Project——Options——Building——C++ Compiler——C++ Compilers(WIN32)——Use classic Borland Compiler设为true。