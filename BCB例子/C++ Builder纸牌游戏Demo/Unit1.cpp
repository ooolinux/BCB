//---------------------------------------------------------------------------

#include <vcl.h>
#include <vector>
#include <algorithm>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
using namespace std;
TForm1 *Form1;
TImage *imgCards[52],*imgBacks[52];
Graphics::TBitmap *bmpBack;
vector<int> cards;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{
    this->Caption="ֽ����Ϸdemo���˿��ƿ��Ե��";
    
    for(int i=0;i<52;i++)
    {
        imgCards[i]=new TImage(this);
        imgCards[i]->Parent=this;
        imgCards[i]->Width=71;
        imgCards[i]->Height=96;
        imgCards[i]->Picture->Bitmap->LoadFromFile("cards_bmp\\bmp"+IntToStr(i+1)+".bmp");
        imgCards[i]->Left=50*(i%13);
        imgCards[i]->Top=10+110*(i/13);
        imgCards[i]->Tag=i;
        imgCards[i]->OnClick=Image1Click;
    }

    bmpBack=new Graphics::TBitmap();
    bmpBack->LoadFromFile("cards_bmp\\bmp64.bmp");

    for(int i=0;i<52;i++)
    {
        imgBacks[i]=new TImage(this);
        imgBacks[i]->Parent=this;
        imgBacks[i]->Picture->Bitmap=bmpBack;
        imgBacks[i]->Left=ClientWidth-80;
        imgBacks[i]->Top=10+i*2;
    }

    cards.reserve(52);
    for(int i=0;i<52;i++)
        cards.push_back(i);

    srand((unsigned)time(NULL));
}
//---------------------------------------------------------------------------
__fastcall TForm1::~TForm1()
{
    for(int i=0;i<52;i++)
    {
        delete imgCards[i];
        delete imgBacks[i];
    }

    delete bmpBack;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::mnuExitClick(TObject *Sender)
{
    Close();    
}
//---------------------------------------------------------------------------

void __fastcall TForm1::mnuNewClick(TObject *Sender)
{
    random_shuffle(cards.begin(),cards.end());
    
    for(int i=0;i<52;i++)
    {
        imgCards[cards[i]]->Left=50*(i%13);
        imgCards[cards[i]]->Top=10+110*(i/13);
        imgCards[cards[i]]->Show();
        imgCards[cards[i]]->BringToFront();
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1Click(TObject *Sender)
{
    TImage *img=(TImage *)Sender;
    int no=img->Tag;
    int row=no/13;
    int col=no%13;
    const AnsiString suits[]={"�ݻ�","����","����","����"};
    AnsiString msg="�������ǣ�"+suits[row];
    switch(col+1)
    {
        case 1:
            msg+="A";
            break;
        case 11:
            msg+="J";
            break;
        case 12:
            msg+="Q";
            break;
        case 13:
            msg+="K";
            break;
        default:
            msg+=IntToStr(col+1);
            break;
    }
    ShowMessage(msg);
    img->Hide();
}
//---------------------------------------------------------------------------

