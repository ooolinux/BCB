object Form1: TForm1
  Left = 244
  Top = 135
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #32440#29260#28216#25103'Demo'
  ClientHeight = 475
  ClientWidth = 764
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 16
    Top = 8
    object mnuGame: TMenuItem
      Caption = #28216#25103'(&G)'
      object mnuNew: TMenuItem
        Caption = #26032#28216#25103'(&N)'
        OnClick = mnuNewClick
      end
      object mnuExit: TMenuItem
        Caption = #36864#20986'(&X)'
        OnClick = mnuExitClick
      end
    end
    object mnuHelp: TMenuItem
      Caption = #24110#21161'(&H)'
      object mnuRule: TMenuItem
        Caption = #35268#21017'(&R)'
      end
      object mnuAbout: TMenuItem
        Caption = #20851#20110'(&A)'
      end
    end
  end
end
