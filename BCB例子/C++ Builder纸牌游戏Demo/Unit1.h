//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
    TMainMenu *MainMenu1;
    TMenuItem *mnuGame;
    TMenuItem *mnuNew;
    TMenuItem *mnuHelp;
    TMenuItem *mnuRule;
    TMenuItem *mnuExit;
    TMenuItem *mnuAbout;
    void __fastcall mnuExitClick(TObject *Sender);
    void __fastcall mnuNewClick(TObject *Sender);
    void __fastcall Image1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TForm1(TComponent* Owner);
    __fastcall TForm1::~TForm1();
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
