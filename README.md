### 图形用户界面（GUI）编程可以学习C++ Builder，架构先进

个人觉得SDK纯API方式编写Windows程序已经过时了，效率太低，了解一下原理就可以了，主要是消息机制。

图形用户界面（GUI）编程可以学习C++ Builder，架构先进（和C#一样拖控件），入门比较容易。

————————————————————

　　C++ Builder是真正的可视化的开发工具。C++ Builder可以用鼠标拖拉控件加上设置属性轻松地“设计”出一个程序的图形用户界面，利用可视的组件/控件像“搭积木”一样，以“所见即所得”的方式搭建应用程序的界面，这也是C++ Builder这个名字中Builder的概念。这样，只需要用C++语言编写业务逻辑代码，类似于DOS下文本界面编程，只要专注于实现业务逻辑功能就可以了，代码非常简洁。数据的输入和表现，都在图形用户界面，非常直观、易用。这就是RAD开发（Rapid Application Development，快速应用程序开发）。

————————————————————

#### 书籍推荐：

《C++Builder 6程序设计教程（第二版）》 （陆卫忠，刘文亮　等编著 /2011-04-01 /科学出版社）（当当网）

《C++Builder 6编程实例精解》（赵明现），PDF，是数字版的，非扫描版的，里面还有一个俄罗斯方块游戏的完整实现。

《精彩C++Builder 6程序设计》（台湾 吴逸贤），里面有10几个简单小游戏的实现，可以自己看懂以后再重写，才是自己掌握的。

《C++ Builder 5高级编程实例精解》（刘滨 编著）都是较大的实用的程序。

————————————————————

#### C++ Builder 6.0几个示例程序项目源代码下载：

————————————————————

C++ Builder第一个程序（模拟聊天室）v1.11

C++ Builder模拟数字双时钟示例（未完成）v0.2.1

C++ Builder纸牌游戏Demo v0.05

C++ Builder不规则图像透明贴图（位图的透明显示）三种方法及简单动画CB10.2 v1.5（资源嵌入）

C++ Builder写的《ZEC 四则运算练习程序》 海底海星情景v0.9.2

C++ Builder简单电话簿Demo例子v1.11

C++ Builder之StringGrid表格简单示例v1.1

————————————————————

#### 程序截图：

<img src="https://gitee.com/ooolinux/BCB/raw/master/BCB%E4%BE%8B%E5%AD%90/%E6%88%AA%E5%9B%BE/%E6%A8%A1%E6%8B%9F%E8%81%8A%E5%A4%A9%E5%AE%A4.png"/>
<img src="https://gitee.com/ooolinux/BCB/raw/master/BCB%E4%BE%8B%E5%AD%90/%E6%88%AA%E5%9B%BE/ZEC%20%E6%A8%A1%E6%8B%9F%E6%95%B0%E5%AD%97%E5%8F%8C%E6%97%B6%E9%92%9F%E5%B8%A6%E6%97%A5%E5%8E%86%E7%A7%92%E8%A1%A8.png"/>
<img src="https://gitee.com/ooolinux/BCB/raw/master/BCB%E4%BE%8B%E5%AD%90/%E6%88%AA%E5%9B%BE/%E7%BA%B8%E7%89%8Cdemo.png"/>
<img src="https://gitee.com/ooolinux/BCB/raw/master/BCB%E4%BE%8B%E5%AD%90/%E6%88%AA%E5%9B%BE/%E4%B8%8D%E8%A7%84%E5%88%99%E5%9B%BE%E5%83%8F%E8%B4%B4%E5%9B%BE.png"/>
<img src="https://gitee.com/ooolinux/BCB/raw/master/BCB%E4%BE%8B%E5%AD%90/%E6%88%AA%E5%9B%BE/ZEC%20%E5%9B%9B%E5%88%99%E8%BF%90%E7%AE%97%E7%BB%83%E4%B9%A0%E7%A8%8B%E5%BA%8F%20%E6%B5%B7%E5%BA%95%E6%B5%B7%E6%98%9F%E6%83%85%E6%99%AF.png"/>
<img src="https://gitee.com/ooolinux/BCB/raw/master/BCB%E4%BE%8B%E5%AD%90/%E6%88%AA%E5%9B%BE/%E7%AE%80%E5%8D%95%E7%94%B5%E8%AF%9D%E7%B0%BFDemo.png"/>
<img src="https://gitee.com/ooolinux/BCB/raw/master/BCB%E4%BE%8B%E5%AD%90/%E6%88%AA%E5%9B%BE/StringGrid%E8%A1%A8%E6%A0%BC%E7%AE%80%E5%8D%95%E7%A4%BA%E4%BE%8B.png"/>



#### 本人博客（书籍、资料、要点）

[https://www.cnblogs.com/ustone/](https://www.cnblogs.com/ustone/)
